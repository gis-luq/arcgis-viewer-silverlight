/*
(c) Copyright ESRI.
This source is subject to the Microsoft Public License (Ms-PL).
Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
All other rights reserved.
*/

using System.Reflection;

[assembly: AssemblyVersion("3.2.0.168")]
[assembly: AssemblyFileVersion("3.2.0.168")]
